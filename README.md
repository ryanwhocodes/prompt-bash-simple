# prompt-bash-simple
Setup for colorising your bash prompt and including the shell, directory and git branch into the prompt PS1

![screenshot](prompt-bash-simple.png)

Inspired by https://unix.stackexchange.com/questions/148/colorizing-your-terminal-and-shell-environment

Add this to the end of your `.bashrc` located in your user home directory.

```bash
parse_git_branch() {
  git branch 2> /dev/null | sed -e '/^[^*]/d' -e 's/* \(.*\)/\1 /'
}

# set the input prompt

ARROW="❯"

# https://stackoverflow.com/questions/19092488/custom-bash-prompt-is-overwriting-itself
# Start and end non printable chars with \[ and \] so shell does not interpret them as
# characters in a line and knows where cursor is

PS1="
\[$(tput bold; tput setaf 2)\]\s\[$(tput sgr0)\] \[$(tput smul; tput setaf 6)\]\W\[$(tput sgr0)\] \[$(tput setaf 3; tput dim)\]$(parse_git_branch)
\[$(tput setaf 5; tput dim)\]$ARROW\[$(tput sgr0)\] "
```

Christmas theme - replace the emojis and colors for other themes
```bash
PS1="
🎄 \[$(tput bold; tput setaf 2)\]\s\[$(tput sgr0)\] ❄️  \[$(tput smul; tput setaf 6)\]\W\[$(tput sgr0)\] 🎁 \[$(tput setaf 3; tput dim)\]$(parse_git_branch)\[$(tput sgr0)\]🌟
\[$(tput setaf 5; tput dim)\]$ARROW\[$(tput sgr0)\] "
```
